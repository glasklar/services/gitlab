# gitlab

This repository collects user instructions and tips & tricks, and is used for tracking operational issues with [this GitLab instance](https://git.glasklar.is/).

----

## User hints

- [Setting up 2FA](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#enable-two-factor-authentication)

- [Retrieving Recovery codes over SSH](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh)

- [Glasklar repository settings](reposettings.md)
