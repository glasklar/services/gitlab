# Authentication and authorization

- [x] Require administrators to enable 2FA (2024-02-16)

- Two-factor grace period: 8 (2024-02-16)

- [x] Allow password authentication for Git over HTTP(S)
  - TODO: Disallow, require keys

- [ ] Enforce two-factor authentication
  - TODO: Enforce, WIP https://git.glasklar.is/glasklar/services/gitlab/-/issues/54

- Remember me
  - [ ] Allow users to extend their session (2024-02-16)

- Dormant users
  - [x] Deactivate dormant users after a period of inactivity (2024-02-16)
  - Days of inactivity before deactivation: 90

- Enabled OAuth authentication sources
  - [x] GitHub (2023-11-27, cf. https://git.glasklar.is/glasklar/services/gitlab/-/issues/22)
  - [x] GitLab.com  (2023-11-27, cf. https://git.glasklar.is/glasklar/services/gitlab/-/issues/22)
