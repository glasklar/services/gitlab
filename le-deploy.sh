#! /usr/bin/bash
set -eu

# Place this file in the directory /etc/letsencrypt/renewal-hooks/deploy/

DESTDIR=/etc/gitlab/ssl
DESTNAME=git.glasklar.is

cd "$RENEWED_LINEAGE"
cp fullchain.pem "${DESTDIR}/${DESTNAME}.crt"
cp privkey.pem "${DESTDIR}/${DESTNAME}.key"

/usr/bin/gitlab-ctl restart nginx
