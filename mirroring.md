# Mirroring

We mirror some Sigsum and ST repositories for discoverability purposes.  See:

  - https://github.com/sigsum/
  - https://github.com/system-transparency

Mirroring of stboot is *known* to be broken, hence why it's marked as "paused"
in the description.  The issue is that there's a large file in the git-history.
See debug notes in https://git.glasklar.is/glasklar/services/gitlab/-/issues/83.

## GitHub

There's a single GitHub account named `sigsum-bot` that owns both the Sigsum and
ST organizations.

The `sigsum-bot` account is configured with an access token, see:

    sigsum-bot
    -> settings
    -> developer settings
    -> personal access tokens
    -> tokens (classic)

The token does not expire, and has "repo" permissions.

**Note:** all relevant sigsum-bot credentials are in passdb.

**Note:** despite the name, `sigsum-bot` is used for both Sigsum and ST.

## GitLab

In the GitLab repo to push-mirror to GitHub:

  1. Go to repo, then Settings -> Repository
  2. Click expand on Mirror repository
  3. Enter Git repository URL: https://github.com/path/to/repo
     - **Note:** The repository needs to exist at GH, create it manually if necessary
  4. Mirror direction: Push
  5. Authentication method: Username and Password
  6. Username: `sigsum-bot`
  7. Password: GitHub access token (see passdb)
  8. Leave the checkboxes unmarked (default)

That's it.  You should see in the same UX that it last mirrored X minutes ago;
and if it was successful.  On error you can click it for a hint of what's wrong.

**Good to know:** sometimes it seems like mirroring isn't super instant, at
least give it a few minutes before starting to troubleshoot.
