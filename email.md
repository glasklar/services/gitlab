# GitLab and email

We are using @incoming.glasklar.is for incoming email, handled by mail.sigsum.org.

While glasklar.is is a domain alias for glasklarteknik.se, incoming.glasklar.is is not a domain alias.

We set up Service Desk email aliases for GitLab projects where we want
to enable creation and interaction with issues and merge requests for
contributors without a GitLab account. See
https://git.glasklar.is/glasklar/services/gitlab/-/issues/61.


