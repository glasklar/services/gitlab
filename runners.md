# Gitlab CI runners at Glasklar, for sysadmins

## Overview

Glasklar runs Gitlab CI runners on a number of VM's.

Each VM is serving one of ST, Sigsum and "the rest".

The actual running is done by the [gitlab-runner][] software, which is
run as a systemd system service under the gitlab-runner user.

The program /usr/bin/gitlab-runner, running as user gitlab-runner,
asks git.glasklar.is for jobs and runs [OCI containers][] using
[podman][] in rootless mode.

podman-system-service(1) is activated by the podman.socket systemd
user unit, owned by user gitlab-runner.

- TODO: configure podman to use our own image registry cache
  - done for podman pull on command line, but doesn't seem to work properly for CI/CD jobs
- TODO: all VM's mount the same S3 bucket for a common file cache

[gitlab-runner]: https://gitlab.com/gitlab-org/gitlab-runner
[OCI containers]: https://opencontainers.org/
[podman]: https://podman.io/

## How to
### How to register a new runner

To register a new runner, log in to a runner VM and do

    sudo -iu gitlab-runner -- gl-runner-reg.sh NAME TOKEN

where NAME is an arbitrary name and TOKEN is the `glrt-*` blurb from
when creating a runner in the Gitlab GUI, like
https://git.glasklar.is/PROJECT/-/runners/new for a project runner.

### How to troubleshoot runners

Is the service running?
```
linus@build-01:~$ sudo systemctl status gitlab-runner | rg 'Loaded:|Active:'
     Loaded: loaded (/etc/systemd/system/gitlab-runner.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-12-03 09:55:03 CET; 15min ago
```

Listing registered runners:
```
linus@build-01:~$ sudo -iu gitlab-runner -- gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=5681 revision=374d34fd version=17.6.0
Listing configured runners                          ConfigFile=/home/gitlab-runner/.gitlab-runner/config.toml
runner2                                             Executor=docker Token=glrt-**REDACTED** URL=https://git.glasklar.is/
user3                                               Executor=docker Token=glrt-**REDACTED** URL=https://git.glasklar.is/
```

Verifying runners:
```
linus@build-01:~$ sudo -iu gitlab-runner -- gitlab-runner verify
Runtime platform                                    arch=amd64 os=linux pid=5701 revision=374d34fd version=17.6.0
WARNING: Running in user-mode.
WARNING: The user-mode requires you to manually start builds processing:
WARNING: $ gitlab-runner run
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

Verifying runner... is valid                        runner=**REDACTED**
Verifying runner... is valid                        runner=**REDACTED**
```

Listing container images:
```
linus@build-01:~$ sudo -iu gitlab-runner -- podman images
REPOSITORY                                                         TAG             IMAGE ID      CREATED       SIZE
registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper  latest          aa86fb539e13  10 hours ago  88.2 MB
registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper  x86_64-v17.6.0  961a109756ad  12 days ago   88.2 MB
docker.io/library/debian                                           bookworm-slim   762d928e7cfb  3 weeks ago   77.8 MB
```

Get a gitlab-runner shell:
```
linus@build-01:~$ sudo -iu gitlab-runner --preserve-env=SHELL -- systemd-run --user --scope --shell
Running scope as unit: run-r0e00d4cd385845a9b80294fbb01bf587.scope
gitlab-runner@build-01:~$
```

## Host setup

This is a mix of "how to" and "what we did manually, once".

The goal is to not have to log in to the builder VM for setting it up
or manage runners, but only for trouble shooting.

### Ansible

The 'builder' ansible role sets up the host for podman, installs the
gitlab-runner package and generates the gl-runner-reg.sh shell script
for a human to execute once per new runner.

### Manual configuration

The following steps are required for setting up a new builder VM.

1. TODO
1. TODO
1. TODO

### Raw "what we did" log
#### Storage
```
root@kvm-03:~# lvcreate -n build-01.glasklarteknik.se-glrun1 -L 128g vg-kvm-03-storage
  Logical volume "build-01.glasklarteknik.se-glrun1" created.
root@kvm-03:~# virsh attach-disk build-01.glasklarteknik.se /dev/vg-kvm-03-storage/build-01.glasklarteknik.se-glrun1 vdb
Disk attached successfully

root@build-01:~# vgcreate vg-storage-1 /dev/vdb
  Physical volume "/dev/vdb" successfully created.
  Volume group "vg-storage-1" successfully created
root@build-01:~# lvcreate -n containers -L 96g vg-storage-1
  Logical volume "containers" created.
root@build-01:~# mkfs.ext4 /dev/vg-storage-1/containers
[snip]
root@build-01:~# mkdir -p /srv/$(hostname -f)/containers
root@build-01:~# blkid /dev/vg-storage-1/containers
/dev/vg-storage-1/containers: UUID="90e3da9e-b17d-43cd-a6c4-7b97bd08b815" BLOCK_SIZE="4096" TYPE="ext4"
root@build-01:~# echo UUID="90e3da9e-b17d-43cd-a6c4-7b97bd08b815" /srv/$(hostname -f)/containers ext4 defaults 0 2 >> /etc/fstab
root@build-01:~# systemctl daemon-reload
root@build-01:~# mount -a
root@build-01:~# df -h /srv/build-01.glasklarteknik.se/containers/
Filesystem                             Size  Used Avail Use% Mounted on
/dev/mapper/vg--storage--1-containers   94G   24K   90G   1% /srv/build-01.glasklarteknik.se/containers

root@build-01:~# systemctl stop gitlab-runner
root@build-01:~# loginctl terminate-user gitlab-runner
root@build-01:~# loginctl user-status gitlab-runner
Failed to get user: User ID 996 is not logged in or lingering
root@build-01:~# mv ~gitlab-runner/.local/share/containers /srv/build-01.glasklarteknik.se/containers/gitlab-runner
root@build-01:~# chmod 700 /srv/build-01.glasklarteknik.se/containers/gitlab-runner
root@build-01:~# chown gitlab-runner:gitlab-runner /srv/build-01.glasklarteknik.se/containers/gitlab-runner

root@build-01:~# : NOTE: i didn't know how to undo loginctl terminate-user so i rebooted instead of doing that and systemctl start gitlab-runner
root@build-01:~# history -a; reboot
```

## Container registry

There is a container registry running on `git.glasklar.is:5050`.

By using images from this registry we avoid hammering other peoples registries.

We keep our images in https://git.glasklar.is/glasklar/infra/containers/container_registry.

To be able to push to this registry, create a [Deploy
token](https://git.glasklar.is/help/user/project/deploy_tokens/index.md#gitlab-deploy-token)

- The token name entered is later used as the username.
- Select role `Developer`.
- Select the following scopes: `read_registry`, `write_registry`.

Use the token as the password when doing `podman login`:

    $ podman login git.glasklar.is:5050
	Username: <token name>
	Password: <token shown in the web GUI>

Example of how to push all the Debian and all the Golang images:

	for t in bookworm trixie sid; do
	  for x in "" -slim -backports; do
	    read -rt 5 -p $t$x?\ ; [ "$REPLY" != "n" ] && \
		    podman push debian:$t$x git.glasklar.is:5050/glasklar/infra/containers/debian:$t$x;
	done; done
    for x in 1.22 latest 1.20-bullseye; do podman push golang:$x git.glasklar.is:5050/glasklar/infra/containers/golang:$x; done
