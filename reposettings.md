# Repository settings

A useful default for the configuration of the default branch should be
set on all top level groups.

- Name: main
- Protected:
  - Allowed to push: No one
  - Allowed to merge: Maintainers
  - Allowed to force push: No
  - Allow developers to push to the initial commit: Yes
