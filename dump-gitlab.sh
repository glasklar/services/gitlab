#! /bin/sh
set -eu

umask 0077

# https://docs.gitlab.com/omnibus/settings/backups.html
BACKUP_CONFIG_DIR=/var/backups/gitlab/config
gitlab-ctl backup-etc --backup-path $BACKUP_CONFIG_DIR > /dev/null

# https://docs.gitlab.com/ee/raketasks/backup_restore.html
gitlab-backup create SKIP=tar,artifacts > /dev/null
