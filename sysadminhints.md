# Viewing logs
- `gitlab-ctl tail`
- `gitlab-ctl tail nginx`
- `ls /var/log/gitlab`

# Checking if upgrade is available
```
apt update ; apt list --upgradable
```

# Upgrading
```
apt update && apt upgrade
false && gitlab-ctl reconfigure  # this seems to be performed by apt upgrade
gitlab-ctl status
```

# Renewing certificates

We're running certbot from cron with `--pre-hook "gitlab-ctl stop
nginx" --post-hook "gitlab-ctl start nginx"` and
[le-deploy.sh](https://git.glasklar.is/glasklar/services/gitlab/-/blob/main/le-deploy.sh)
as deploy hook (in `/etc/letsencrypt/renewal-hooks/deploy/10-deploy.sh`).


Example extending SAN's manually:
```
certbot certonly --standalone --pre-hook "gitlab-ctl stop nginx" --post-hook "gitlab-ctl start nginx" -d gitlab-01.sigsum.org,gitlab.glasklarteknik.se,gitlab.sigsum.org,git.glasklar.is
```

# Storage

The storage layout is a little whimsical, I'm sorry about that.

But here's the gist: Three PV's with a single VG each, from three
different KVM host VG's:

| VG            | PV        | KVM VG             |
|---------------|-----------|--------------------|
| gitlab-01-vg  | /dev/vda5 | logsrv-01-ssd      |
| gitlab-vg     | /dev/vdb  | logsrv-01-nvme     |
| gitlab-01-var | /dev/vdc  | logsrv-01-vg (SSD) |

See https://git.glasklar.is/glasklar/services/gitlab/-/issues/35 for
pieces of info on what's growing under `/var`.

# Monitoring

We allow internal networks to access the [health check monitoring endpoints](https://docs.gitlab.com/ee/administration/monitoring/health_check.html).

# Email

Gitlab sends email using SMTP FROM `gitlab@glasklarteknik.se` thanks
to the `gitlab_email_from` setting in `/etc/gitlab/gitlab.rb`. (It's
possible that the 822 `From:` header is always set to
`gitlab@glasklarteknik.se` as well.)

# Artifact max size

- https://docs.gitlab.com/ee/administration/settings/continuous_integration.html

- 2024-01-26
  - maximum artifact size set to 1 GB for system-transparency/core
  - removed old setting of 500 MB on system-transparency/core/system-transparency

# Nginx max sizes

- `nginx['client_max_body_size'] = '500m'` means that we can upload 500M

# Rate limits

As of 2024-10-10 the following rate limits are enabled
(https://git.glasklar.is/admin/application_settings/network). This on
top of default rate limits.

- unauthenticated API request: 3600/3600
- unauthenticated web request: 3600/3600
- authenticated API request: 14400/3600
- authenticated web request: 7200/3600
- unauthenticated Git HTTP request: 3600/3600

# Runners

See [Runners](runners.md).
