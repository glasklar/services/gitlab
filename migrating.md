# Migrating

When migrating a remote from cgit, don't forget to also migrate branches.  One
way to do this manually is to checkout the branch on the old cgit remote, then
push it to the new gitlab remote with the same branch name.  There is probably a
better way to do this automatically, but was good enough for a few branches.

It also seems like we did not migrate tags probably.  That's TODO if we want to.
One reason why we might not care is because we only have tags in Go repos, and
get/install on those for `sigsum.org/repo` should fail (mismatched module name).
